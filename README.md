# `jitsi-web`

## Configuration

Matomo can be configured using environment variables:

- `MATOMO_ENABLE`: required to be `true` to enable Matomo support,
- `MATOMO_ENDPOINT`: enter the full URL of your Matomo instance,
- `MATOMO_SITE_ID`: the site ID you got from your Matomo instance
